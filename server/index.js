// Server
const express = require('express')
const morgan = require('morgan')
const routes = require('./routes/router.js')
const path = require('path')

const app = express()

// TODO Disponer carpeta de React
app.use(morgan('dev'))
app.use(express.static(path.join(__dirname, '../client/build')))

app.set('port', process.env.PORT || 3000)

app.listen(app.get('port'), () => {
  console.log(`Server listening on port 3000`)
})